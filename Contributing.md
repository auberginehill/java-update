# Contributing

|        |                           |                                                                                                               |
|:------:|---------------------------|---------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/java-update/issues/). |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/java-update/issues/). |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/java-update/pull-requests/). |




## Issue Template

Please ensure to be running at least PowerShell 2.0 before submitting an issue.

Information required for troubleshooting:

  * OS & Build Number
  * PowerShell Version
  * User Privileges (User/Admin)
  * Error Messages, if any

### Expected Behavior


### Actual Behavior


### Steps to Reproduce the Problem

  1. 
  1. 
  1. 


### Specifications

  * Version:
  * Platform:
  * Subsystem:




---

## Feature Request Template

### About
I have this amazing suggestion/enhancement (and may want to implement it)... or just this tiny little thing that could be beneficary to this project.

### Is your feature request related to a problem? Please describe.
A clear and concise description of what the problem is. Ex. I have an issue when...

### A Description of the Solution You'd Like
A clear and concise description of what you want to happen. Add any considered drawbacks.

### The Alternatives You've Considered
A clear and concise description of any alternative solutions or features you've considered.

### Teachability, Documentation, Adoption and Migration Strategy
If you can, explain how users will be able to use this and possibly write out a version the docs.
Maybe a screenshot or design?




---

## Pull Request Template

Please, go through at least a majority of these steps before you submit a PR.

  1. Make sure that your PR is not a duplicate. Also, make sure that...
  1. ...you have done your changes in a separate branch. Branches MUST have descriptive names that start with either the `fix/` or `feature/` prefixes. Good examples are: `fix/signin-issue` or `feature/issue-templates`.
  1. ...you have a descriptive commit message with a short title (first line).
  1. ...you have only one commit (if not, squash them into one commit).
  1. Make sure that `npm test` doesn't throw any error. If it does, fix them first and amend your commit (`git commit --amend`).

**After** these steps, you're ready to open a pull request. Then:

  1. Your pull request should usually not target the `master` branch on this repository. You probably want to target `staging` instead.
  1. Give a descriptive title to your PR.
  1. Provide a description of your changes.
  1. Put `closes #XXXX` in your comment to auto-close the issue that your PR fixes (if such).




---

## Pull Request Process

  1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.
  1. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
  1. Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent.
  1. You may merge the Pull Request in once you have the sign-off of two other developers, or if you do not have permission to do that, you may request the second reviewer to merge it for you.




---

## Answer Template

### Observations 

  - Observed
  - Appears

### Diagnosis

  1. Inserted 
  1. Confluence

### Diagnostic Steps

  - Replicate
    1. Navigate
    1. Click
  - Generate
  - Run
    - If

### Cause

  - Attachment
  - Images 

### Resolution

  - Query
  - Flush