
## Java-Update.ps1

|                  |                                                                                                            |
|------------------|------------------------------------------------------------------------------------------------------------|
| **OS:**          | Windows                                                                                                    |
| **Type:**        | A Windows PowerShell script                                                                                |
| **Language:**    | Windows PowerShell                                                                                         |
| **Status:**      | Partially obsolete, please see https://github.com/auberginehill/java-update/issues/1 for further details. Java-Update.ps1 doesn't seem to work with JRE Family Version 9 or later versions of Java. JRE Family Version 8 is deemed to be "the latest" by this script, which only seems to be factually true concerning the 32-bit Java versions. |
| **Description:** | Java-Update downloads a list of the most recent Java version numbers against which it compares the Java version numbers found on the system and displays, whether a Java update is needed or not. The actual update process naturally needs elevated rights, and if a working Internet connection is not found, Java-Update will exit at Step 8. Java-Update detects the installed Javas by querying the Windows registry for installed programs. The keys from `HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\` and `HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\` are read on 64-bit computers, and on 32-bit computers only the latter path is accessed. |
|                  | Java-Update tries to write several Java-related configuration files at an early stage, the "`deployment.properties`" -file in Step 4 and an Install Configuration File in Step 5 (`java_config.txt`). In Step 6, if enough rights are granted (run as an administrator) Java-Update tries to remove the excessive duplicate Java versions, so that only those Java versions, which are deemed to be the latest, would remain. Usually only one instance of Java will remain, but if both the 32-bit and 64-bit Javas have the same latest version number, both of the Java versions (32-bit and 64-bit) will be preserved. At this stage the Java Auto Updater will also be uninstalled. In Step 6 the `msiexec.exe` is called to uninstall old Javas, so the process runs at a normal pace. |
|                  | If Java-Update is run without elevated rights (but with a working Internet connection) in a machine with old Java versions, it will be shown that a Java update is needed, but Java-Update will exit at Step 13 before actually downloading any files. To perform an update with Java-Update, PowerShell has to be run in an elevated window (run as an administrator). |
|                  | If Java-Update is run in an elevated PowerShell window and no Java is detected, the script offers the option to install Java in the "**Admin Corner**", where, in contrary to the main autonomous nature of Java-Update, an end-user input is required. |
|                  | If Java-Update is run with elevated rights (with a working Internet connection) in a machine with old Java versions, Java-Update tries first to remove the excessive duplicate Java versions (at Step 6), and in the update procedure (if the most recent non-beta Java version is not detected and Java-Update is run with administrative rights) Java-Update downloads the Java uninstaller from Oracle/Sun (a file which is not used with this script) and a full Java offline installer from Sun (the 64-bit Java for a 64-bit machine and the 32-bit Java for a 32-bit machine). After stopping several Java-related processes Java-Update uninstalls the outdated Java versions in two phases (Java Auto Updater first and then the other Javas as listed by `Get-WmiObject -Class Win32_InstalledWin32Program` command) with the `msiexec.exe /uninstall` command and installs the downloaded Java version. |
|                  | For further information, please see the [Wiki](https://bitbucket.org/auberginehill/java-update/wiki/).     |
| **Homepage:**    | https://bitbucket.com/auberginehill/java-update                                                            |
|                  | Short URL: https://tinyurl.com/yy8cjoyo                                                                    |
| **Version:**     | 2.0                                                                                                        |
| **Downloads:**   | For instance [Java-Update.ps1](https://bitbucket.org/auberginehill/java-update/src/master/Java-Update.ps1). Or [everything as a .zip-file](https://bitbucket.org/auberginehill/java-update/downloads/). Or `git clone https://auberginehill@bitbucket.org/auberginehill/java-update.git`. Or `git fetch && git checkout master`. |




### Screenshot

![Screenshot](https://bitbucket.org/auberginehill/java-update/raw/5f324f36c66386a92f759606c41bec1a3f88d3d9/Java-Update.png)




### www

|                        |                                                                              |                               |
|:----------------------:|------------------------------------------------------------------------------|-------------------------------|
| :globe_with_meridians: | [Script Homepage](https://github.com/auberginehill/java-update)              |                               |
|                        | [Test Internet connection](http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20110612212629/http://powershell.com/cs/blogs/tips/archive/2011/05/04/test-internet-connection.aspx)) | ps1 |
|                        | [PowerTips Monthly vol 8 January 2014](http://powershell.com/cs/PowerTips_Monthly_Volume_8.pdf#IDERA-1702_PS-PowerShellMonthlyTipsVol8-jan2014) (or one of the [archive.org versions](https://web.archive.org/web/20150110213108/http://powershell.com/cs/media/p/30542.aspx)) | Tobias Weltner |
|                        | [How to run exe with/without elevated privileges from PowerShell](http://stackoverflow.com/questions/29266622/how-to-run-exe-with-without-elevated-privileges-from-powershell?rq=1) | alejandro5042 |
|                        | [What's the best way to determine the location of the current PowerShell script?](http://stackoverflow.com/questions/5466329/whats-the-best-way-to-determine-the-location-of-the-current-powershell-script?noredirect=1&lq=1) | JaredPar and Matthew Pirocchi |
|                        | [Adding a Simple Menu to a Windows PowerShell Script](https://technet.microsoft.com/en-us/library/ff730939.aspx) | Microsoft TechNet |
|                        | [Creating a Menu](http://powershell.com/cs/forums/t/9685.aspx) (or one of the [archive.org versions](https://web.archive.org/web/20150910111758/http://powershell.com/cs/forums/t/9685.aspx)) | lamaar75 |
|                        | [Powershell show elapsed time](http://stackoverflow.com/questions/10941756/powershell-show-elapsed-time) | Jeff |
|                        | [Perfect Progress Bars for PowerShell](https://www.credera.com/blog/technology-insights/perfect-progress-bars-for-powershell/) |   |
|                        | [Powershellv2 - remove last x characters from a string](http://stackoverflow.com/questions/27175137/powershellv2-remove-last-x-characters-from-a-string#32608908) |   |
|                        | [http://www.figlet.org/](http://www.figlet.org/) and [ASCII Art Text Generator](http://www.network-science.de/ascii/) | ASCII Art |
|                        | [HTML To Markdown Converter](https://digitalconverter.azurewebsites.net/HTML-to-Markdown-converter) |        |
|                        | [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables) |                               |
|                        | [HTML table syntax into Markdown](https://jmalarcon.github.io/markdowntables/) |                             |
|                        | [A HTML to Markdown converter written in JavaScript](https://domchristie.github.io/turndown/) | Dom Christie |
|                        | [Paste to Markdown](https://euangoddard.github.io/clipboard2markdown/)       |                               |
|                        | [Convert HTML or anything to Markdown](https://cloudconvert.com/html-to-md)  |                               |
