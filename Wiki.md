```
       _                         _    _           _       _
      | |                       | |  | |         | |     | |
      | | __ ___   ____ _ ______| |  | |_ __   __| | __ _| |_ ___
  _   | |/ _` \ \ / / _` |______| |  | | '_ \ / _` |/ _` | __/ _ \
 | |__| | (_| |\ V / (_| |      | |__| | |_) | (_| | (_| | ||  __/
  \____/ \__,_| \_/ \__,_|       \____/| .__/ \__,_|\__,_|\__\___|
                                       | |
                                       |_|
```

# Java-Update Wiki

[TOC]

## Screenshot

![Screenshot](https://bitbucket.org/auberginehill/java-update/raw/5f324f36c66386a92f759606c41bec1a3f88d3d9/Java-Update.png)




## Outputs 

:arrow_right: Displays Java related information in console. 

  - Tries to remove excessive (duplicate) Java versions and update the one remaining instance of Java to the latest non-beta version (the 64-bit Java for a 64-bit machine and the 32-bit Java for a 32-bit machine), if old Java(s) is/are found, if Java-Update is run in an elevated Powershell window and if a working Internet connection is detected. In addition to that...

  - The Java Deployment Configuration File (`deployment.properties`) is altered with new parameters, if it is found in one of its default Windows locations, and the following backups are made.
  - To see the actual values that are being written, please see the Step 4 in the [script](https://bitbucket.org/auberginehill/java-update/src/master/Java-Update.ps1) itself, where the following values are added or forced (overwritten) upon the original settings:

  - An Install Configuration File is created in Step 5.

  - To see the actual values that are being written, please see the Step 5 in the [script](https://bitbucket.org/auberginehill/java-update/src/master/Java-Update.ps1) itself, where the following values are written:

  - After the installation the downloaded files (uninstaller and the install file) are not purged from the `$path` directory.

  - Additionally two auxiliary csv-files are created at `$path` and during the actual update procedure a log-file is also created to the same location.

  - To open these file locations in a Resource Manager Window, for instance a command...

    `Invoke-Item ([string][Environment]::GetFolderPath("LocalApplicationData") + 'Low\Sun\Java\Deployment')`

    or

    `Invoke-Item ([string][Environment]::GetFolderPath("ApplicationData") + '\Sun\Java\Deployment')`

    or

    `Invoke-Item ("\$env:temp")`

    ...may be used at the PowerShell prompt window `[PS\>]`.




## Notes 

:warning: Requires a working Internet connection for downloading a list of the most recent Java version numbers.

  - Also requires a working Internet connection for downloading a Java uninstaller and a complete Java installer from Oracle/Sun (but this procedure is not initiated, if the system is deemed up-to-date). The download location URLs of the full installation files seem not to follow any pre-determined format anymore. The download locations of full installation files for both 32-bit and 64-bit Java versions are determined at Step 10 and Step 11.

  - For performing any actual updates with Java-Update, it's mandatory to run this script in an elevated PowerShell window (where PowerShell has been started with the 'run as an administrator' option). The elevated rights are needed for uninstalling Java(s) and installing Java.

  - Please also notice that during the actual update phase Java-Update closes a bunch of processes without any further notice in Step 18 and may do so also in Step 6. Please also note that Java-Update alters the system files at least in Steps 4, 5, 19 and 24, so that for instance, all successive Java installations (even the ones not initiated by this Java-Update script) will be done "silently" i.e. without any interactive pages or prompts.

  - Please note that when run in an elevated PowerShell window and old Java(s) is/are detected, Java-Update will automatically try to uninstall them and download files from the Internet without prompting the end-user beforehand or without asking any confirmations (in Step 6 and from Step 16 onwards).

  - The notoriously slow and possibly harmful `Get-WmiObject -Class Win32\_Product` command is deliberately not used for listing the installed Javas or for performing uninstallations despite the powerful Uninstall-method associated with this command, since the `Win32\_Product` Class has some unpleasant behaviors – namely it uses a provider DLL that validates the consistency of every installed MSI package on the computer (`msiprov.dll` with the mandatorily initiated resiliency check, in which the installations are verified and possibly also repaired or repair-installed), which is the main reason behind [the](https://sdmsoftware.com/group-policy-blog/wmi/why-win32_product-is-bad-news/) [slow](https://blogs.technet.microsoft.com/askds/2012/04/19/how-to-not-use-win32_product-in-group-policy-filtering/) [performance](https://support.microsoft.com/en-us/kb/974524) of this command. All in all `Win32\_product` Class is not query optimized and in Java-Update a combination of various registry queries, `msiexec.exe` and `Get-WmiObject -Class Win32\_InstalledWin32Program` is used instead.

  - Please note that the downloaded files are placed in a directory, which is specified with the `$path` variable (at line 15). The `$env:temp` variable points to the current temp folder. The default value of the `\$env:temp` variable is `C:\Users<username\>\AppData\Local\Temp` (i.e. each user account has their own separate temp folder at path `%USERPROFILE%\AppData\Local\Temp`). To see the current temp path, for instance a command...

    `[System.IO.Path]::GetTempPath()`

    ...may be used at the PowerShell prompt window `[PS\>]`.

  - To change the temp folder for instance to `C:\Temp`, please, for example, follow the instructions at [Temporary Files Folder - Change Location in Windows](http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html), which in essence are something along the lines:
    1. Right click Computer icon and select Properties (or select Start → Control Panel → System. On Windows 10 this instance may also be found by right clicking Start and selecting Control Panel → System... or by pressing `[Win-key]` + X and selecting Control Panel → System). On the window with basic information about the computer...
    1. Click on Advanced system settings on the left panel and select Advanced tab on the "System Properties" pop-up window.
    1. Click on the button near the bottom labeled Environment Variables.
    1. In the topmost section, which lists the User variables, both TMP and TEMP may be seen. Each different login account is assigned its own temporary locations. These values can be changed by double clicking a value or by highlighting a value and selecting Edit. The specified path will be used by Windows and many other programs for temporary files. It's advisable to set the same value (a directory path) for both TMP and TEMP.
    1. Any running programs need to be restarted for the new values to take effect. In fact, probably Windows itself needs to be restarted for it to begin using the new values for its own temporary files.




## Examples

:book: To open this code in Windows PowerShell, for instance:

  1. `./Java-Update`

    Runs the script. Please notice to insert `./` or `.\` before the script name.

  1. `help ./Java-Update -Full`

    Displays the help file.

  1. `New-Item -ItemType File -Path C:\Temp\Java-Update.ps1`

    Creates an empty ps1-file to the `C:\Temp` directory. The `New-Item` cmdlet has an inherent `-NoClobber` mode built into it, so that the procedure will halt, if overwriting (replacing the contents) of an existing file is about to happen. Overwriting a file with the `New-Item` cmdlet requires using the `Force`. If the path name and/or the filename includes space characters, please enclose the whole `-Path` parameter value in quotation marks (single or double): `New-Item -ItemType File -Path "C:\Folder Name\Java-Update.ps1"`. For more information, please type "`help New-Item -Full`".

  1. `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine`

    This command is altering the Windows PowerShell rights to enable script execution in the default (`LocalMachine`) scope, and defines the conditions under which Windows PowerShell loads configuration files and runs scripts in general. In Windows Vista and later versions of Windows, for running commands that change the execution policy of the `LocalMachine` scope, Windows PowerShell has to be run with elevated rights (Run as Administrator). The default policy of the default (`LocalMachine`) scope is "`Restricted`", and a command "`Set-ExecutionPolicy Restricted`" will "undo" the changes made with the original example above (had the policy not been changed before...). Execution policies for the local computer (`LocalMachine`) and for the current user (`CurrentUser`) are stored in the registry (at for instance the `HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy` key), and remain effective until they are changed again. The execution policy for a particular session (`Process`) is stored only in memory, and is discarded when the session is closed.

    |                | PowerShell Execution Policy Parameters                                         |
    |----------------|--------------------------------------------------------------------------------|
    | `Restricted`   | Does not load configuration files or run scripts, but permits individual commands. `Restricted` is the default execution policy. |
    | `AllSigned`    | Scripts can run. Requires that all scripts and configuration files be signed by a trusted publisher, including the scripts that have been written on the local computer. Risks running signed, but malicious, scripts. |
    | `RemoteSigned` | Requires a digital signature from a trusted publisher on scripts and configuration files that are downloaded from the Internet (including e-mail and instant messaging programs). Does not require digital signatures on scripts that have been written on the local computer. Permits running unsigned scripts that are downloaded from the Internet, if the scripts are unblocked by using the `Unblock-File` cmdlet. Risks running unsigned scripts from sources other than the Internet and signed, but malicious, scripts. |
    | `Unrestricted` | Loads all configuration files and runs all scripts. Warns the user before running scripts and configuration files that are downloaded from the Internet. Not only risks, but actually permits, eventually, runningany unsigned scripts from any source. Risks running malicious scripts. |
    | `Bypass`       | Nothing is blocked and there are no warnings or prompts. Not only risks, but actually permits running any unsigned scripts from any source. Risks running malicious scripts. |
    | `Undefined`    | Removes the currently assigned execution policy from the current scope. If the execution policy in all scopes is set to `Undefined`, the effective execution policy is `Restricted`, which is the default execution policy. This parameter will not alter or remove the ("master") execution policy that is set with a Group Policy setting. |
    | **Notes:**     | Please note that the Group Policy setting "`Turn on Script Execution`" overrides the execution policies set in Windows PowerShell in all scopes. To find this ("master") setting, please, for example, open the Local Group Policy Editor (`gpedit.msc`) and navigate to Computer Configuration → Administrative Templates → Windows Components → Windows PowerShell. The Local Group Policy Editor (`gpedit.msc`) is not available in any Home or Starter edition of Windows. |
    |                | For more information, please type "`Get-ExecutionPolicy -List`", "`help Set-ExecutionPolicy -Full`", "`help about_Execution_Policies`" or visit [Set-ExecutionPolicy](https://technet.microsoft.com/en-us/library/hh849812.aspx) or [About Execution Policies](http://go.microsoft.com/fwlink/?LinkID=135170). |




## Contributing

|        |                           |                                                                                                                        |
|:------:|---------------------------|------------------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/java-update/issues/).               |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/java-update/issues/).   |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/java-update/pull-requests/). |

For further information, please see the [Contributing.md](https://bitbucket.org/auberginehill/java-update/wiki/Contributing.md) page.




## Wiki Features

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

```
$ git clone https://auberginehill@bitbucket.org/auberginehill/java-update.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.